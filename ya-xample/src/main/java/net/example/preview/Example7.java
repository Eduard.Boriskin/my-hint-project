package net.example.preview;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Example7 {

    public static void main(String[] args) throws Exception {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

        Integer J = Integer.valueOf(r.readLine());
        String S = r.readLine();

        List<Character> input = S.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        Map<Character, List<Integer>> charsIndex = new TreeMap<>();

        for (int i = 0; i < input.size(); i++) {
            Character character = input.get(i);

            List<Integer> list = charsIndex.getOrDefault(character, new ArrayList<>());
            list.add(i);
            charsIndex.put(character, list);
        }
        List<Character> characters = List.of(charsIndex.keySet().toArray(new Character[0]));

        int countReplace = J;

        List<String> strRes = new ArrayList<>();
        strRes.add(S);
        for (Character character : characters) {
            countWithReplace(strRes, character, input, countReplace);
        }

        System.out.println(strRes);
    }

    static void countWithReplace(List<String> strRes, Character character, List<Character> input, final int countReplace) {
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < input.size(); i++) {

            Character ch = input.get(i);



        }


    }


}
