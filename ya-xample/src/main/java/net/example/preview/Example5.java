package net.example.preview;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Example5 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Integer input = Integer.parseInt(sc.nextLine());
        String inputArray = sc.nextLine();

        List<Integer> collect =
                Arrays.stream(inputArray.split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        Integer sum = collect.stream().reduce(0, Integer::sum);

        if (Collections.max(collect) * 2 - sum > 0) {
            System.out.println(Collections.max(collect) * 2 - sum);
        } else {
            System.out.println(sum);
        }
    }
}
