package net.example.preview;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Example4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String count = sc.nextLine();
        int[][] board = new int[10][10];

        List<Dec> decList = new ArrayList<>();
        for (int i = 1; i <= Integer.parseInt(count); i++) {
            String str = sc.nextLine();
            String[] split = str.split(" ");

            int posX = Integer.parseInt(split[0]);
            int posY = Integer.parseInt(split[1]);

            decList.add(new Dec(posX, posY));
            board[posX][posY] = 1;
        }

        int res = 0;
        for (Dec dec: decList) {
            if (board[dec.getX() + 1][dec.getY()] == 0){
                res++;
            }
            if (board[dec.getX() - 1][dec.getY()] == 0){
                res++;
            }
            if (board[dec.getX()][dec.getY() - 1] == 0){
                res++;
            }
            if (board[dec.getX()][dec.getY() + 1] == 0){
                res++;
            }
//            System.out.println(dec + " res:" + res);
          }
        System.out.println(res);
    }

}
class Dec {

    private int x;
    private int y;

    public Dec(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}

