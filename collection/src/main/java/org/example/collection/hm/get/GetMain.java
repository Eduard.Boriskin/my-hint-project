package org.example.collection.hm.get;

import java.util.HashMap;

public class GetMain {
    public static void main(String[] args) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("test", "default");

        //execute base,  print default
        System.out.println(hashMap.getOrDefault("test", getTestBase()));

        //execute base, print base
        System.out.println(hashMap.getOrDefault("test1", getTestBase()));

        //print default
        System.out.println(hashMap.computeIfAbsent("test", s -> getTestBase()));

        //execute base, print base
        System.out.println(hashMap.computeIfAbsent("test1", s -> getTestBase()));

        //true
        System.out.println(hashMap.containsKey("test1"));

    }

    public static String getTestBase() {
        System.out.print("execute base, ");
        return "base";
    }
}
