package net.example;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BaseRegExpTest {

    @Test
    public void test() {
        List<String> list = getStringList();
        int matches = 0;

        Pattern pattern = Pattern.compile("A");

        for (String str: list) {
            Matcher matcher = pattern.matcher(str);
            if (matcher.find()){
                matches++;
            }
        }

        assertEquals(1, matches);
    }


    private List<String> getStringList() {
        List<String> result = new ArrayList<>();
        result.add("Alise");
        result.add("Bob");
        result.add("Mike");

        return result;
    }
}
