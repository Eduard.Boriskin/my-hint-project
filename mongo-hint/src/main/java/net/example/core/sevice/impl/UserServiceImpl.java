package net.example.core.sevice.impl;

import lombok.RequiredArgsConstructor;
import net.example.core.model.entity.User;
import net.example.core.repository.UserRepository;
import net.example.core.sevice.UserService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User saveUser(User user) {
        return userRepository.insert(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
}
