package net.example.core.sevice;

import net.example.core.model.entity.User;

import java.util.List;

public interface UserService {

    User saveUser(User user);

    List<User> getAll();
}
