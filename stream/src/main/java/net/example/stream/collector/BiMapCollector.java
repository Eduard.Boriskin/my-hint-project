package net.example.stream.collector;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Sets;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class BiMapCollector implements Collector<Data, HashBiMap<String, String>, BiMap<String, String>> {

    @Override
    public Supplier<HashBiMap<String, String>> supplier() {
        return HashBiMap::create;
    }

    @Override
    public BiConsumer<HashBiMap<String, String>, Data> accumulator() {
        return (k,v) -> {
            k.put(v.getId(), v.getUsername());
        };
    }

    @Override
    public BinaryOperator<HashBiMap<String, String>> combiner() {
        return (first, second) -> {
            first.putAll(second);
            return first;
        };
    }

    @Override
    public Function<HashBiMap<String, String>, BiMap<String, String>> finisher() {
        return (k) -> k;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Sets.immutableEnumSet(Characteristics.UNORDERED);
    }
}