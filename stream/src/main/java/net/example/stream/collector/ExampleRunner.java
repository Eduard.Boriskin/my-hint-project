package net.example.stream.collector;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ExampleRunner {

    public static void main(String[] args) {

        HashBiMap<String, String> map = getInitData().stream()
                .collect(Collectors.toMap(
                        Data::getId,
                        Data::getUsername,
                        (existing, replacement) -> replacement,
                        HashBiMap::create
                ));

        System.out.println(map.get("1"));
        System.out.println(map.inverse().get("test1"));
        System.out.println(map.get("2"));
        System.out.println(map.inverse().get("test2"));

        BiMap<String, String> mapBi = getInitData().stream()
                .collect(new BiMapCollector());

        System.out.println(mapBi.get("1"));
        System.out.println(mapBi.inverse().get("test1"));
        System.out.println(mapBi.get("2"));
        System.out.println(mapBi.inverse().get("test2"));

    }

    private static List<Data> getInitData() {
        List<Data> dataList = new ArrayList<>();

        dataList.add(Data.builder().username("test1").id("1").build());
        dataList.add(Data.builder().username("test2").id("2").build());
        dataList.add(Data.builder().username("test3").id("3").build());
        dataList.add(Data.builder().username("test4").id("4").build());

        return dataList;
    }
}

@Getter
@Builder
class Data {

    private String id;

    private String username;

}
