package net.example.lib.exception;

public class CoreException extends RuntimeException{

    public CoreException(String message){
        super(message);
    }

    public static Throwable getRootCause(Throwable t) {
        Throwable result = t;
        Throwable cause;

        while (null != (cause = result.getCause()) && (result != cause)) {
            result = cause;
        }
        return result;
    }

}
