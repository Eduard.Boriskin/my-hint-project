package net.example.lib.rest;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.example.lib.exception.impl.RestCommunicationException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


import java.util.*;

import static net.example.lib.exception.CoreException.getRootCause;


@Slf4j
@RequiredArgsConstructor
public class RestComponent {

    private final RestTemplate restTemplate;

    public <T> T getData(Class<T> classType, String url, MultiValueMap<String, String> bodyMap, Map<String, String> headersMap, HttpMethod httpMethod) throws RestCommunicationException {
        ResponseEntity<T> response = getDataResponse(classType, url, bodyMap, headersMap, httpMethod);
        return response == null ? null : response.getBody();
    }

    public <T> ResponseEntity<T> getDataResponse(Class<T> classType, String url, MultiValueMap<String, String> bodyMap, Map<String, String> headersMap, HttpMethod httpMethod) throws RestCommunicationException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        if (headersMap != null && !headersMap.isEmpty()) {
            headersMap.forEach(headers::set);
        }

        ResponseEntity<T> response;
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(bodyMap, headers);
        log.info("Request [{}] url: {}", httpMethod, url);
        try {
            response = restTemplate.exchange(url, httpMethod, entity, classType);
        } catch (HttpStatusCodeException e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), e.getStatusCode());
        } catch (ResourceAccessException e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    public <T, E> ResponseEntity<T> getDataResponse(Class<T> classType, String url, E body, Map<String, String> headersMap, HttpMethod httpMethod) throws RestCommunicationException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        if (headersMap != null && !headersMap.isEmpty()) {
            headersMap.forEach(headers::set);
        }

        ResponseEntity<T> response;
        HttpEntity<E> entity = new HttpEntity<>(body, headers);
        log.info("Request [{}] url: {}", httpMethod, url);
        try {
            response = restTemplate.exchange(url, httpMethod, entity, classType);
        } catch (HttpStatusCodeException e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), e.getStatusCode());
        } catch (ResourceAccessException e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    public <T, E> ResponseEntity<T> getDataResponse(ParameterizedTypeReference<T> classType, String url, E body, Map<String, String> headersMap, HttpMethod httpMethod) throws RestCommunicationException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        if (headersMap != null && !headersMap.isEmpty()) {
            headersMap.forEach(headers::set);
        }

        ResponseEntity<T> response;
        HttpEntity<E> entity = new HttpEntity<>(body, headers);
        log.info("Request [{}] url: {}", httpMethod, url);
        try {
            response = restTemplate.exchange(url, httpMethod, entity, classType);
        } catch (HttpStatusCodeException e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), e.getStatusCode());
        } catch (ResourceAccessException e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }


    public String createUrl(String mainUrl, String endpointUrl, Map<String, String> mapParameter) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(mainUrl).path(endpointUrl);
        if (!CollectionUtils.isEmpty(mapParameter)) {
            mapParameter.forEach((key, value) -> {
                if (value != null) {
                    builder.queryParam(key, value);
                }
            });
        }
        return builder.build().toUriString();
    }

    public String createUrlWithListParameters(String mainUrl, String endpointUrl, Map<String, List<Object>> mapParameterList) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(mainUrl).path(endpointUrl);
        if (!CollectionUtils.isEmpty(mapParameterList)) {
            mapParameterList.forEach((key, values) -> {
                if (!CollectionUtils.isEmpty(values)) {
                    for (Object value : values) {
                        builder.queryParam(key, value);
                    }
                }
            });
        }
        return builder.build().toUriString();
    }

    public <T> List<T> getDataList(Class<T[]> classType, String url, String body, Map<String, String> headersMap, HttpMethod httpMethod) throws RestCommunicationException {
        ResponseEntity<T[]> response = getDataListResponse(classType, url, body, headersMap, httpMethod);
        return Optional.ofNullable(response)
                .map(ResponseEntity::getBody)
                .map(Arrays::asList)
                .orElse(Collections.emptyList());
    }

    public <T> ResponseEntity<T[]> getDataListResponse(Class<T[]> classType, String url, String body, Map<String, String> headersMap, HttpMethod httpMethod) throws RestCommunicationException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        if (headersMap != null && !headersMap.isEmpty()) {
            headersMap.forEach(headers::set);
        }
        HttpEntity<String> entity = new HttpEntity<>(body, headers);
        log.info("Request [{}] url: {}", httpMethod, url);
        try {
            return restTemplate.exchange(url, httpMethod, entity, classType);
        } catch (HttpStatusCodeException e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), e.getStatusCode());
        } catch (ResourceAccessException e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            throw new RestCommunicationException(getRootCause(e).getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
